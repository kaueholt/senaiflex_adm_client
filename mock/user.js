import { param2Obj } from './utils'
import { getId } from '@/utils/auth'

const tokens = {
  admin: {
    token: 'admin-token'
  },
  vendor: {
    token: 'vendor-token'
  },
  manager: {
    token: 'manager-token'
  }
}

const users = {
  'admin-token': {
    roles: ['admin'],
    introduction: 'I am a super administrator',
    // avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    // avatar: '../static/admin.png',
    avatar: '../static/user.png',
    name: 'Super Admin',
    idPessoa: getId(),
    tipoPessoa: '9',
  },
  'vendor-token': {
    roles: ['vendor'],
    introduction: 'I am a vendor',
    avatar: '../static/user.png',
    name: 'Vendedor',
    idPessoa: getId(),
    tipoPessoa: '2',
  },
  'manager-token': {
    roles: ['manager'],
    introduction: 'I am a manager',
    avatar: '../static/user.png',
    name: 'Coordenador',
    idPessoa: getId(),
    tipoPessoa: '3',
  }
}


export default {
  login: res => {
    const { username } = JSON.parse(res.body)
    const data = tokens[username]

    if (data) {
      return {
        code: 20000,
        data
      }
    }
    return {
      code: 60204,
      message: 'Login / Senha incorretos.'
    }
  },
  getInfo: res => {
    const { token } = param2Obj(res.url)
    const info = users[token]
    if (info) {
      return {
        code: 20000,
        data: info
      }
    }
    return {
      code: 50008,
      message: 'Falha ao obter dados do usuário.'
    }
  },
  logout: () => {
    return {
      code: 20000,
      data: 'success'
    }
  }
}
