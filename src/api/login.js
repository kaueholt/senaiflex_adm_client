import request from '@/utils/request'
import { removeToken } from '@/utils/auth'

export function login(username, password) {
  const cnpjCpf = username
  return request({
    // url: '/user/login',
    url: '/auth/login-admin',
    method: 'post',
    data: {
      username,
      cnpjCpf,
      password
    }
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  removeToken()
  return request({
    url: '/user/logout',
    method: 'post'
  })
}
