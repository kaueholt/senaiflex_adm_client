
export default {
  methods: {
    /**
     * Redireciona para a rota de edicao do item
     * @param {object} item item a ser editado
     */
    handleUpdate(item) {
      this.$router.push(`${item.id}`)
    },
    /**
     * Remove o item, necessita da confirmacao do usuario
     * @param {object} item item a ser removido
     */
    handleDelete(item) {
      this.$confirm('Deseja remover este item?', 'Excluir')
        .then((ok) => ok ? this.delete(item) : false)
        .then(() => this.$notify({
          title: 'Item excluído',
          message: 'OK',
          type: 'success'
        })).catch((err) => err != 'cancel' ?
          this.$notify({
            title: 'Falha ao exluir',
            message: 'O item possui vínculos com outras tabelas.',
            type: 'error',
            duration: 5000,
          }) : null
        )
    },
    handleDeletePaginated(item, prototypeOf) {
      let mensagem = 'Deseja remover este item?'
      item['id'] ? mensagem += ' ID: ' + item['id'] : null
      item['nome'] ? mensagem += ' Nome: ' + item['nome'] : null
      this.$confirm(mensagem, 'Excluir')
        .then((ok) => ok ? this.deletePaginated(item) : false)
        .then(() => this.$notify({
          title: 'Item excluído',
          message: 'OK',
          type: 'success'
        })).catch((err) => err != 'cancel' ?
          this.$notify({
            title: 'Falha ao exluir',
            message: err.response ? err.response.data ? err.response.data.message ? err.response.data.message : err.response.data : err.response : err.message ? err.message : err ? err : (prototypeOf + ' possui vínculos com outras tabelas.'),
            type: 'error',
            duration: 5000,
          }) : null
        )
    },
    // handleDeletePessoaPaginated(item,query,coordenadorID,vendedorID,cidade) {
    //   let cidadeId = cidade ? cidade.id ? cidade.id : '' : ''
    //   const obj = {name: query, coordenador: coordenadorID, vendedor: vendedorID, cidade:cidadeId}
    //   console.log('handleDeletePessoaPaginated')
    //   console.log(obj)
    //   this.$confirm('Deseja remover este item?', 'Excluir')
    //     .then((ok) => ok ? this.deletePessoaPaginated(item, 1) : false)
    //     .then(() => this.$notify({
    //       title: 'Pessoa removida',
    //       message: 'OK',
    //       type: 'success'
    //     }))
    //     .catch(console.log)
    // },
    /**
     * Valida e submete o formulario $refs['dataForm'], utiliza o this.item para enviar para upsert
     */
    submit() {
      this.$refs['dataForm'].clearValidate()
      this.$refs['dataForm'].validate(valid => {
        if (valid) {
          let msg = typeof this.item.id != 'undefined' ? 'Item atualizado' : 'Item inserido'
          this.upsert(this.item)
            .then(() => this.$notify({ title: msg, message: 'OK', type: 'success', duration: 2000 }))
            .then(() => this.$router.go(-1))
            .catch((err) => this.$notify({ title: 'Ocorreu um erro', message: err.response ? err.response.data ? err.response.data.message ? err.response.data.message : err.response.data : err.response : err.message ? err.message : err, type: 'error', duration: 2000 }))
        } else {
          this.$notify({ title: 'Revise os campos', message: 'Erro na validacao', type: 'error', duration: 2000 })
        }
      })
    }
  }
}
