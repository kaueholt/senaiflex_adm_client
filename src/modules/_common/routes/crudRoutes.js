import Layout from '@/views/layout/Layout'

const crudRoutes = (title, name, icon, hidden) => {
  let nameWithoutAdmin = name.substr(-6) === '-admin' ? name.substr(0,(name.length-6)) : name
  return {
    path: `/${nameWithoutAdmin}`,
    redirect: `/${nameWithoutAdmin}_list`,
    component: Layout,
    meta: {
      title: title,
      icon: icon
    },
    hidden: hidden ? hidden : false,
    children: [
      {
        path: 'list',
        name: `${nameWithoutAdmin}_list`,
        component: () => import(`@/modules/${nameWithoutAdmin}/views/index`),
        meta: { title: title, noCache: true }
      },
      {
        path: 'new',
        name: `${nameWithoutAdmin}_new`,
        hidden: true,
        component: () => import(`@/modules/${nameWithoutAdmin}/views/upsert`),
        meta: { title: `Cadastro de ${title}`, noCache: true }
      },
      {
        path: 'newI',
        name: `${nameWithoutAdmin}_newI`,
        hidden: true,
        component: () => import(`@/modules/${nameWithoutAdmin}/views/upsertIndustria`),
        meta: { title: `Cadastro de Indústria`, noCache: true }
      },
      {
        path: ':id',
        name: `${nameWithoutAdmin}_edit`,
        hidden: true,
        component: () => import(`@/modules/${nameWithoutAdmin}/views/upsert`),
        meta: { title: `Editar ${title}`, noCache: true }
      }
    ]
  }
}

export { crudRoutes }

