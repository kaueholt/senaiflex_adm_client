
const crudRoutes = (title, name, icon) => {
let nameWithoutAdmin = name.substr(-6) === '-admin' ? name.substr(0,(name.length-6)) : name
  return [
    {
      path: `/${name}/list`,
      name: `${nameWithoutAdmin}_list`,
      component: () => import(`@/modules/${nameWithoutAdmin}/views/index`),
      meta: { title: title, noCache: true, icon: icon }
    },
    {
      path: `/${name}/new`,
      name: `${nameWithoutAdmin}_new`,
      hidden: true,
      component: () => import(`@/modules/${nameWithoutAdmin}/views/upsert`),
      meta: { title: `Cadastro de ${title}`, noCache: true }
    },
    {
      path: `/${name}/:id`,
      name: `${nameWithoutAdmin}_edit`,
      hidden: true,
      component: () => import(`@/modules/${nameWithoutAdmin}/views/upsert`),
      meta: { title: `Editar ${title}`, noCache: true }
    }
  ]
}

export { crudRoutes }

