/** @param {BaseService} Service */
const CRUD = (Service) => {
  /** @template T */
  const state = {
    /**
     * @type {T[]}
     * Lista de todos os {T}
    */
    items: [],
    propostas: [],
    cursos: [],
    conteudos: [],
    cidades: [],
    pessoas: [],
    selectedItem: undefined,
    page: 1,
    total: 0,
    from: 0,
    to: 0,
    lastPage: 0,
    loading: false,
    loadingXLS: false
  }

  const getters = {
    items: (state) => state.items,
    propostas: (state) => state.propostas,
    cursos: (state) => state.cursos,
    conteudos: (state) => state.conteudos,
    cidades: (state) => state.cidades,
    pessoas: (state) => state.pessoas,
    selectedItem: (state) => state.selectedItem,
    loading: (state) => state.loading,
    loadingXLS: (state) => state.loadingXLS,
    page: (state) => state.page,
    from: (state) => state.from,
    to: (state) => state.to,
    lastPage: (state) => state.lastPage,
    total: (state) => state.total
  }

  /** @type {import("vuex").MutationTree<typeof state>} */
  const mutations = {
    SET_ITEMS(state, payload) {
      state.items = payload
    },
    SET_CURSOS(state, payload) {
      state.cursos = payload
    },
    SET_CONTEUDOS(state, payload) {
      state.conteudos = payload
    },
    SET_CIDADES(state, payload) {
      state.cidades = payload
    },
    SET_PESSOAS(state, payload) {
      state.pessoas = payload
    },
    SET_SELECTED_ITEM(state, payload) {
      state.selectedItem = payload
    },
    SET_LOADING(state, payload) {
      state.loading = payload
    },
    SET_LOADINGXLS(state, payload) {
      state.loadingXLS = payload
    },
    SET_PAGE(state, payload) {
      state.page = payload
    },
    SET_FROM(state, payload) {
      state.from = payload
    },
    SET_TO(state, payload) {
      state.to = payload
    },
    SET_LAST_PAGE(state, payload) {
      state.lastPage = payload
    },
    SET_PROPOSTAS(state, payload) {
      state.propostas = payload
    },
    SET_TOTAL(state, payload) {
      state.total = payload
    },
    SET_ALL(state, payload) {
      state.items = payload.data
      state.page = 1
      state.total = payload.total
      state.from = payload.from
      state.to = payload.to
      state.lastPage = payload.lastPage
    },
  }

  /** @type {import("vuex").ActionTree<typeof state>} */
  const actions = {
    /**
     * Buscar lista de itens e adicionar o resultado na state list
     * @param {*} context
     */
    find(context) {
      context.commit('SET_LOADING', true)
      return Service.find()
        .then((response) => context.commit('SET_ITEMS', response.data))
        .finally(() => context.commit('SET_LOADING', false))
    },
    findUnpaginated(context) {
      context.commit('SET_LOADING', true)
      return Service.findUnpaginated()
        .then((response) => context.commit('SET_ITEMS', response.data))
        .finally(() => context.commit('SET_LOADING', false))
    },
    findUnpaginatedCurso(context) {
      context.commit('SET_LOADINGXLS', true)
      return Service.findUnpaginated()
        .then((response) => context.commit('SET_CURSOS', response.data))
        .finally(() => context.commit('SET_LOADINGXLS', false))
    },
    findUnpaginatedPessoa(context) {
      context.commit('SET_LOADINGXLS', true)
      return Service.findUnpaginated()
        .then((response) => context.commit('SET_PESSOAS', response.data))
        .finally(() => context.commit('SET_LOADINGXLS', false))
    },
    findUnpaginatedConteudo(context) {
      context.commit('SET_LOADINGXLS', true)
      return Service.findUnpaginated()
        .then((response) => context.commit('SET_CONTEUDOS', response.data))
        .finally(() => context.commit('SET_LOADINGXLS', false))
    },
    findUnpaginatedCidade(context) {
      context.commit('SET_LOADINGXLS', true)
      return Service.findUnpaginated()
        .then((response) => context.commit('SET_CIDADES', response.data))
        .finally(() => context.commit('SET_LOADINGXLS', false))
    },
    findUnpaginatedProposta(context) {
      context.commit('SET_LOADING', true)
      return Service.findUnpaginated()
        .then((response) => context.commit('SET_PROPOSTAS', response.data))
        .finally(() => context.commit('SET_LOADING', false))
    },
    // findUnpaginatedPessoa(context) {
    //   context.commit('SET_LOADING', true)
    //   return Service.findUnpaginated()
    //     .then((response) => context.commit('SET_PESSOAS', response.data))
    //     .finally(() => context.commit('SET_LOADING', false))
    // },
    /**
     * Buscar lista de itens e adicionar o resultado na state list
     * @param {*} context
     */
    findPaginated(context, { page, name, descricao, nome, cidade, filtroVendedoresAtivo, filtroCoordenadoresAtivo, coordenador, vendedor }) {
      page = page || 1
      const query = {
        page
      }
      if (cidade){
        query.cidade = cidade
      }
      if (filtroVendedoresAtivo){
        query.filtroVendedoresAtivo = filtroVendedoresAtivo
      }
      if (filtroCoordenadoresAtivo){
        query.filtroCoordenadoresAtivo = filtroCoordenadoresAtivo
      }
      if (vendedor){
        query.vendedor = vendedor
      }
      if (coordenador){
        query.coordenador = coordenador
      }
      if (name) {
        query.name = name
      }
      if (nome) {
        query.nome = nome
      }
      if (descricao) {
        query.descricao = descricao
      }
      context.commit('SET_LOADING', true)
      context.commit('SET_PAGE', page)
      return Service.find(query)
        .then((response) => {
          context.commit('SET_ITEMS', response.data.data)
          context.commit('SET_TOTAL', response.data.total)
          context.commit('SET_FROM', response.data.from)
          context.commit('SET_TO', response.data.to)
          context.commit('SET_LAST_PAGE', response.data.last_page)
        })
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     * Executa o find passando parametros de query na rota T/params
     * @param {*} context
     */
    params(context, payload) {
      context.commit('SET_LOADING', true)
      return Service.params(payload)
        .then((response) => context.commit('SET_ITEMS', response.data))
        .finally(() => context.commit('SET_LOADING', false))
    },
    paramsProposta(context, payload) {
      context.commit('SET_LOADING', true)
      return Service.params(payload)
        .then((response) => context.commit('SET_ALL', response.data))
        .finally(() => context.commit('SET_LOADING', false))
    },
    search(context, payload) {
      context.commit('SET_LOADING', true)
      return Service.search(payload)
        .then((response) => response.data)
        .finally(() => context.commit('SET_LOADING', false))
    },
    searchIndustria(context, payload) {
      context.commit('SET_LOADING', true)
      return Service.searchIndustria(payload)
        .then((response) => response.data)
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     *  Enviar requisição para adicionar um novo item na base
     * @param {*} context
     * @param {T} payload
     */
    create(context, payload) {
      context.commit('SET_LOADING', true)
      return Service.create(payload)
        //.then(() => context.dispatch('find'))
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     *  Enviar requisição para atualizar ou inserir item na base
     * @param {*} context
     * @param {T} payload
     */
    upsert(context, payload) {
      context.commit('SET_LOADING', true)
      let promise = null
      if (payload.id) {
        const { id } = payload
        delete payload.id
        promise = Service.update(id, payload)
      } else {
        promise = Service.create(payload)
      }
      return promise
        //.then(() => context.dispatch('find'))
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     * Enviar requisição para atualizar um item na base
     * O payload precisa conter 2 objetos, um com o id do item que está sendo editado, outro com os dados
     * @param {*} context
     * @param {*} payload
     */
    update(context, { id, data }) {
      context.commit('SET_LOADING', true)
      return Service.update(id, data)
        //.then(() => context.dispatch('find'))
        .finally(() => context.commit('SET_LOADING', false))
    },
    /**
     * Remover o item {T}
     * @param {*} context
     * @param {T} payload
     */
    delete(context, { id }) {
      context.commit('SET_LOADING', true)
      return Service.delete(id)
        .then(() => context.dispatch('find'))
        .finally(() => context.commit('SET_LOADING', false))
    },
    deletePaginated(context, { id }) {
      context.commit('SET_LOADING', true)
      return Service.deletePaginated(id)
        .then((response) => context.commit('SET_ALL', response.data))
        .finally(() => context.commit('SET_LOADING', false))
    },
    // deletePessoaPaginated(context, { id, obj }) {
    //   console.log('crudstore deletePessoaPaginated')
    //   console.log(obj)
    //   context.commit('SET_LOADING', true)
    //    return Service.deletePessoaPaginated(id, obj)
    //     .then((response) => context.commit('SET_ALL', response.data))
    //     .finally(() => context.commit('SET_LOADING', false))
    // },
    /**
     *  Seleciona um item {T}
     * @param {*} context
     * @param {T} payload
     */
    select(context, { id }) {
      context.commit('SET_LOADING', true)
      return Service.findOne(id)
        .then((response) => response.data)
        .then((data) => {
          context.commit('SET_SELECTED_ITEM', data)
          return data
        })
        .finally(() => context.commit('SET_LOADING', false))
    }
  }

  return {
    state,
    getters,
    mutations,
    actions
  }
}

export { CRUD }
