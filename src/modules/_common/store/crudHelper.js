import { mapActions, mapGetters } from 'vuex'

export default (MODULE_NAME) => {
  /**
   * @typedef {object} TComputed
   * @property {{(): T[]}} items
   * Retorna a lista de items {T}
   * @property {{(): string}} selectedItem
   * Retorna o item selecionado {T}
   * @property {{(): boolean}} loading
   * Retorna true quando ha alguma operacao em andamento
   */
  /** @type {TComputed} */
  const computed = {
    ...mapGetters({
      items: `${MODULE_NAME}/items`,
      selectedItem: `${MODULE_NAME}/selectedItem`,
      cursos: 'curso/cursos',
      conteudos: 'conteudo/conteudos',
      loading: `${MODULE_NAME}/loading`,
      loadingXLS: `${MODULE_NAME}/loadingXLS`,
      total: `${MODULE_NAME}/total`,
      from: `${MODULE_NAME}/from`,
      to: `${MODULE_NAME}/to`,
      totalPages: `${MODULE_NAME}/totalPages`
    })
  }
  /**
   * @typedef {object} TActions
   * @property {{() => Promise<void>}} find
   * Carrega a Lista de itens T na store
   * @property {{(page: number) => Promise<void>}} findPaginated
   * Carrega a Lista de itens T na store
   * @property {{() => Promise<void>}} params
   * Carrega a Lista de itens T na store, passando os parametros
   * @property {{({ id: any }): Promise<void>}} delete
   * Remove o item T
   * @property {{({ id: any }): Promise<void>}} select
   * Seleciona o objeto com id carregando-o e deixando-o disponivel em selectedItem
   * @property {{(object: T): Promise<void>}} upsert
   * Insere ou atualiza o elemento
   */
  /** @type {TActions} */
  // unpaginated: `${MODULE_NAME}/unpaginated`,
  const actions = {
    ...mapActions({
      find: `${MODULE_NAME}/find`,
      findPaginated: `${MODULE_NAME}/findPaginated`,
      findUnpaginated: `${MODULE_NAME}/unpaginated`,
      findUnpaginatedCurso: `${MODULE_NAME}/unpaginated`,
      findUnpaginatedPessoa: `${MODULE_NAME}/unpaginated`,
      findUnpaginatedConteudo: `${MODULE_NAME}/unpaginated`,
      findUnpaginatedCidade: `${MODULE_NAME}/unpaginated`,
      params: `${MODULE_NAME}/params`,
      paramsProposta: `${MODULE_NAME}/paramsProposta`,
      delete: `${MODULE_NAME}/delete`,
      deletePaginated: `${MODULE_NAME}/deletePaginated`,
      deletePessoaPaginated: `${MODULE_NAME}/deletePessoaPaginated`,
      select: `${MODULE_NAME}/select`,
      upsert: `${MODULE_NAME}/upsert`
    })
  }
  return {
    computed,
    actions
  }
}
