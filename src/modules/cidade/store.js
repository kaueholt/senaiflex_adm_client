import { CRUD } from '@/modules/_common/store/crud'
import Service from '@/modules/_common/api/BaseService'

const crud = CRUD(new Service('/cidade'))

const module = {
  namespaced: true,
  modules: {
    crud
  },
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  }
}

export default module
