import Service from '@/modules/_common/api/BaseService'
import { getToken, getId} from '@/utils/auth'

export default class PessoaService extends Service {
  constructor() {
    super('/pessoa-admin')
  }

  // findUnpaginated() {
  //   if(getToken() === 'admin-token'){
  //     this.axios.get(`${process.env.BASE_API}/pessoa-admin/unpaginated`)
  //   }
  // }

  findCoordenadores() {
    let query = ''
    if(getToken() === 'admin-token'){
      return this.axios.get(`${process.env.BASE_API}/coordenador-admin`)
    }
    if(getToken() === 'manager-token'){
      query = 'coordenador='+getId()
      return this.axios.get(`${process.env.BASE_API}/coordenador-admin?${query}`)
    }
    if(getToken() === 'vendor-token'){
      query = 'vendedor='+getId()
      return this.axios.get(`${process.env.BASE_API}/coordenador-admin?${query}`)
    }
  }

  findVendedores(coordenador) {
  let query = ''
    if(getToken() === 'admin-token'){
      if (coordenador) {
        query = `coordenador=${coordenador}`
      }
      return this.axios.get(`${process.env.BASE_API}/vendedor-admin?${query}`)
    }
    if(getToken() === 'manager-token'){
      query = 'coordenador='+getId()
      return this.axios.get(`${process.env.BASE_API}/vendedor-admin?${query}`)
    }
    if(getToken() === 'vendor-token'){
      query = 'vendedor='+getId()
      return this.axios.get(`${process.env.BASE_API}/vendedor-admin?${query}`)
    }
  }
}
