import { crudRoutes } from '@/modules/_common/routes/crudRoutes'

export default crudRoutes('Pessoas', 'pessoa-admin', 'user')
