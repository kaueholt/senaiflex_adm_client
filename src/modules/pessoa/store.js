import { CRUD } from '@/modules/_common/store/crud'

import PessoaService from './service'
const service = new PessoaService()
const crud = CRUD(service)

const module = {
  namespaced: true,
  modules: {
    crud
  },
  state: {
    idPessoa: '',
    vendedor: {},
    coordenador: {},
    vendedores: [],
    coordenadores: [],
    user: {
      role: '',
      username: ''
    },
  },
  getters: {
    idPessoa: (state) => state.idPessoa,
    vendedores: (state) => state.vendedores,
    coordenadores: (state) => state.coordenadores,
    vendedor: (state) => state.vendedor,
    coordenador: (state) => state.coordenador,
	  role: (state) => state.user.role,
  },
  mutations: {
    SET_IDPESSOA(state, payload) {
      state.idPessoa = payload
    },
    SET_COORDENADORES(state, payload) {
      state.coordenadores = payload
    },
    SET_VENDEDORES(state, payload) {
      state.vendedores = payload
    },
    SET_VENDEDOR(state, payload) {
      state.vendedor = payload
    },
    SET_COORDENADOR(state, payload) {
      state.coordenador = payload
    },
    SET_ROLE(state, payload) {
      state.user.username = payload.username
      state.user.role = payload.role
    },
    SET_SELECTED_ITEM(state, payload) {
      if (payload.data.vendedor){
        let dataAuxVendedor = payload.data.vendedor.dataVigenciaInicial ? new Date(payload.data.vendedor.dataVigenciaInicial) : null
        let dataFinalAuxVendedor = payload.data.vendedor.dataVigenciaFinal ? new Date(payload.data.vendedor.dataVigenciaFinal) : null
        payload.data.vendedor.dataVigenciaInicial = new Date(payload.data.vendedor.dataVigenciaInicial).setDate(dataAuxVendedor.getDate() + 1)
        payload.data.vendedor.dataVigenciaInicial = new Date(payload.data.vendedor.dataVigenciaInicial)
        dataFinalAuxVendedor ? payload.data.vendedor.dataVigenciaFinal = new Date(payload.data.vendedor.dataVigenciaFinal).setDate(dataFinalAuxVendedor.getDate() + 1) : null
        dataFinalAuxVendedor ? payload.data.vendedor.dataVigenciaFinal = new Date(payload.data.vendedor.dataVigenciaFinal) : null
      }
      if (payload.data.coordenador){
        let dataAuxCoordenador = payload.data.coordenador.dataVigenciaInicial ? new Date(payload.data.coordenador.dataVigenciaInicial) : null
        let dataFinalAuxCoordenador = payload.data.coordenador.dataVigenciaFinal ? new Date(payload.data.coordenador.dataVigenciaFinal) : null
        payload.data.coordenador.dataVigenciaInicial = new Date(payload.data.coordenador.dataVigenciaInicial).setDate(dataAuxCoordenador.getDate() + 1)
        payload.data.coordenador.dataVigenciaInicial = new Date(payload.data.coordenador.dataVigenciaInicial)
        dataFinalAuxCoordenador ? payload.data.coordenador.dataVigenciaFinal = new Date(payload.data.coordenador.dataVigenciaFinal).setDate(dataFinalAuxCoordenador.getDate() + 1) : null
        dataFinalAuxCoordenador ? payload.data.coordenador.dataVigenciaFinal = new Date(payload.data.coordenador.dataVigenciaFinal) : null
      }
      state.selectedItem = payload
    },
  },
  actions: {
    /**
     *  Enviar requisição para adicionar um novo item na base
     * @param {*} context
     * @param {T} payload
     */
    setVendedor(context, payload){
      context.commit('SET_VENDEDOR',payload)
    },
    setIdPessoa(context, payload){
      context.commit('SET_IDPESSOA',payload)
    },

    createAdmin(context, payload) {
      context.commit('SET_LOADING', true)
      return service.post('admin', payload)
        .finally(() => context.commit('SET_LOADING', false))
    },


    updateAdmin(context, payload) {
      context.commit('SET_LOADING', true)
      return service.post('updateAdmin', payload)
        .finally(() => context.commit('SET_LOADING', false))
    },

    findCoordenadores(context, payload) {
      context.commit('SET_LOADING', true)
      return service.findCoordenadores() // service.post('admin', payload)
        .then((response) => context.commit('SET_COORDENADORES', response.data))
        .finally(() => context.commit('SET_LOADING', false))
    },
    findVendedores(context, payload) {
      context.commit('SET_LOADING', true)
      return service.findVendedores(payload) // service.post('admin', payload)
        .then((response) => context.commit('SET_VENDEDORES', response.data))
        .finally(() => context.commit('SET_LOADING', false))
    }
  }
}

export default module
