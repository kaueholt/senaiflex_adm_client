import { CRUD } from '@/modules/_common/store/crud'
import Service from '@/modules/_common/api/BaseService'

const crud = CRUD(new Service('/categoria'))

const module = {
  namespaced: true,
  modules: {
    crud
  },
  state: {
  },
  getters: {
  },
  mutations: {
    SET_SELECTED_ITEM(state, payload) {
      let dataAux = new Date(payload.dataVigenciaInicio)
      let dataFinalAux = payload.dataVigenciaFinal ? new Date(payload.dataVigenciaFinal) : null
      payload.dataVigenciaInicio = new Date(payload.dataVigenciaInicio).setDate(dataAux.getDate() + 1)
      payload.dataVigenciaInicio = new Date(payload.dataVigenciaInicio)
      dataFinalAux ? payload.dataVigenciaFinal = new Date(payload.dataVigenciaFinal).setDate(dataFinalAux.getDate() + 1) : null
      dataFinalAux ? payload.dataVigenciaFinal = new Date(payload.dataVigenciaFinal) : null
      state.selectedItem = payload
    },
  },
  actions: {
  }
}

export default module
