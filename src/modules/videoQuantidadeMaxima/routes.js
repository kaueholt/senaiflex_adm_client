import { crudRoutes } from '@/modules/_common/routes/crudRoutes'
import { getToken } from '@/utils/auth' // getToken from cookie

export default crudRoutes(
  'Vídeos (Qtd. Máx.)',
  'videoQuantidadeMaxima',
  'video',
  getToken() === 'admin-token' ? false : true
)
