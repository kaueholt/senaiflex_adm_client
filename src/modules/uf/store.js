import { CRUD } from '@/modules/_common/store/crud'
import Service from '@/modules/_common/api/BaseService'

const service = new Service('/uf')
const crud = CRUD(service)

const module = {
  namespaced: true,
  modules: {
    crud
  },
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
    /**
     *  Enviar requisição para atualizar ou inserir item na base
     * @param {*} context
     * @param {T} payload
     */
    upsertUF(context, payload) {
      context.commit('SET_LOADING', true)
      let promise = null
      if (payload.id) {
        const { uf } = payload
        delete payload.id
        promise = service.update(uf, payload)
      } else {
        promise = service.create(payload)
      }
      return promise
        .then(() => context.dispatch('find'))
        .finally(() => context.commit('SET_LOADING', false))
    }
  }
}

export default module
