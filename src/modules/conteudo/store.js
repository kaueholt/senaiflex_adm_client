import { CRUD } from '@/modules/_common/store/crud'
import Service from '@/modules/_common/api/BaseService'

const crud = CRUD(new Service('/conteudo'))

const module = {
  namespaced: true,
  modules: {
    crud
  },
  state: {
  },
  getters: {
  },
  mutations: {
    SET_SELECTED_ITEM(state, payload) {
      let dataAux = new Date(payload.dataVigenciaInicial)
      let dataFinalAux = payload.dataVigenciaFinal ? new Date(payload.dataVigenciaFinal) : null
      payload.dataVigenciaInicial = new Date(payload.dataVigenciaInicial).setDate(dataAux.getDate() + 1)
      payload.dataVigenciaInicial = new Date(payload.dataVigenciaInicial)
      dataFinalAux ? payload.dataVigenciaFinal = new Date(payload.dataVigenciaFinal).setDate(dataFinalAux.getDate() + 1) : null
      dataFinalAux ? payload.dataVigenciaFinal = new Date(payload.dataVigenciaFinal) : null
      state.selectedItem = payload
    },
  },
  actions: {
  }
}

export default module
