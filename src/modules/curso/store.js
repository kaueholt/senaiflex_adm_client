import { CRUD } from '@/modules/_common/store/crud'
import Service from '@/modules/_common/api/BaseService'

const crud = CRUD(new Service('/curso'))
// cursos: []

// cursos: (state) => state.cursos

// SET_CURSOS(state, payload) {
    //   state.cursos = payload
    // },

const module = {
  namespaced: true,
  modules: {
    crud
  },
  state: {
  },
  getters: {

  },
  mutations: {
    SET_SELECTED_ITEM(state, payload) {
      let dataAux = new Date(payload.dataVigenciaInicial)
      let dataFinalAux = payload.dateVigenciaFinal ? new Date(payload.dateVigenciaFinal) : null
      payload.dataVigenciaInicial = new Date(payload.dataVigenciaInicial).setDate(dataAux.getDate() + 1)
      payload.dataVigenciaInicial = new Date(payload.dataVigenciaInicial)
      dataFinalAux ? payload.dateVigenciaFinal = new Date(payload.dateVigenciaFinal).setDate(dataFinalAux.getDate() + 1) : null
      dataFinalAux ? payload.dateVigenciaFinal = new Date(payload.dateVigenciaFinal) : null
      state.selectedItem = payload
    },

  },
  actions: {
  }
}

export default module
