import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import '@/styles/theme/index.scss'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/pt-br' // lang i18n
import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'
import VueRouterUserRoles from "vue-router-user-roles";

Vue.use(VueRouterUserRoles, { router });

import '@/icons' // icon
import '@/permission' // permission control

/**
 * This project originally used easy-mock to simulate data,
 * but its official service is very unstable,
 * and you can build your own service if you need it.
 * So here I use Mock.js for local emulation,
 * it will intercept your request, so you won't see the request in the network.
 * If you remove `../mock` it will automatically request easy-mock data.
 */
import '../mock' // simulation data

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faTachometerAlt } from '@fortawesome/free-solid-svg-icons'

library.add(faTachometerAlt)

Vue.component('font-awesome-icon', FontAwesomeIcon)

// Initialize Firebase

// firebase.initializeApp(config)

Vue.use(ElementUI, { locale })

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

import storePlugin from 'store-plugin'

var firebase = {
  apiKey: 'AIzaSyCGAL6a0WU4DiHXFtxnehijFJSBFybN5r4',
  authDomain: 'exy-exo.firebaseapp.com',
  databaseURL: 'https://exy-exo.firebaseio.com',
  projectId: 'exy-exo',
  storageBucket: 'exy-exo.appspot.com',
  messagingSenderId: '404562436113'
}

Vue.use(storePlugin, {
  store,
  firebase
})

import excel from 'vue-excel-export'
Vue.use(excel)
