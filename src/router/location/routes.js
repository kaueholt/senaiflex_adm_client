import Layout from '@/views/layout/Layout'
import pais from '@/modules/pais/routes'
import uf from '@/modules/uf/routes'
import cidade from '@/modules/cidade/routes'
import sede from '@/modules/sede/routes'
import regiao from '@/modules/regiao/routes'
import microRegiao from '@/modules/microRegiao/routes'
import { getToken } from '@/utils/auth' // getToken from cookie

export default {
  path: '/localizacao',
  component: Layout,
  redirect: '/sede/list',
  name: 'Localidades',
  hidden: getToken() === 'admin-token' ? false : true,
  meta: { title: 'Localidades', icon: 'locale' },
  children: [
    ...pais,
    ...uf,
    ...cidade,
    ...sede,
    ...regiao,
    ...microRegiao
  ]
}
