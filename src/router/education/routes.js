import Layout from '@/views/layout/Layout'
import curso from '@/modules/curso/routes'
import conteudo from '@/modules/conteudo/routes'
import categoria from '@/modules/categoria/routes'
import area from '@/modules/area/routes'
import { getToken } from '@/utils/auth' // getToken from cookie

export default {
  path: '/educacao',
  component: Layout,
  redirect: '/curso/list',
  hidden: getToken() === 'admin-token' ? false : true,
  meta: { title: 'Educação' , icon: 'educacao' },
  children: [
    ...curso,
    ...conteudo,
    ...categoria,
    ...area
  ]
}
