import router from './router'
import store from './store'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { Message } from 'element-ui'
import { getToken } from '@/utils/auth' // getToken from cookie

NProgress.configure({ showSpinner: false })// NProgress configuration

const whiteList = ['/login'] // 不重定向白名单
router.beforeEach((to, from, next) => {
  NProgress.start()
  const token = getToken()
  if (token) {
    // console.log(to.path.toLowerCase())
    // console.log(from.path.toLowerCase())
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done() // if current page is dashboard will not trigger	afterEach hook, so manually handle it
    }else if((to.path.toLowerCase() === '/conteudo/list' || to.path.toLowerCase() === '/conteudo/new' || to.path.toLowerCase() === '/conteudo/edit' || to.path.toLowerCase() === '/categoria/list' || to.path.toLowerCase() === '/categoria/new' || to.path.toLowerCase() === '/categoria/edit' || to.path.toLowerCase() === '/area/list' || to.path.toLowerCase() === '/area/new' || to.path.toLowerCase() === '/area/edit' || to.path.toLowerCase() === '/pais/list' || to.path.toLowerCase() === '/pais/new' || to.path.toLowerCase() === '/pais/edit' || to.path.toLowerCase() === '/uf/list' || to.path.toLowerCase() === '/uf/new' || to.path.toLowerCase() === '/uf/edit' || to.path.toLowerCase() === '/cidade/list' || to.path.toLowerCase() === '/cidade/new' || to.path.toLowerCase() === '/cidade/edit' || to.path.toLowerCase() === '/sede/list' || to.path.toLowerCase() === '/sede/new' || to.path.toLowerCase() === '/sede/edit' || to.path.toLowerCase() === '/regiao/list' || to.path.toLowerCase() === '/regiao/new' || to.path.toLowerCase() === '/regiao/edit' || to.path.toLowerCase() === '/curso/list' || to.path.toLowerCase() === '/curso/new' || to.path.toLowerCase() === '/curso/edit') && token === 'admin-token'){
      next()
    }else if((to.path.toLowerCase() === '/conteudo/list' || to.path.toLowerCase() === '/conteudo/new' || to.path.toLowerCase() === '/conteudo/edit' || to.path.toLowerCase() === '/categoria/list' || to.path.toLowerCase() === '/categoria/new' || to.path.toLowerCase() === '/categoria/edit' || to.path.toLowerCase() === '/area/list' || to.path.toLowerCase() === '/area/new' || to.path.toLowerCase() === '/area/edit' || to.path.toLowerCase() === '/pais/list' || to.path.toLowerCase() === '/pais/new' || to.path.toLowerCase() === '/pais/edit' || to.path.toLowerCase() === '/uf/list' || to.path.toLowerCase() === '/uf/new' || to.path.toLowerCase() === '/uf/edit' || to.path.toLowerCase() === '/cidade/list' || to.path.toLowerCase() === '/cidade/new' || to.path.toLowerCase() === '/cidade/edit' || to.path.toLowerCase() === '/sede/list' || to.path.toLowerCase() === '/sede/new' || to.path.toLowerCase() === '/sede/edit' || to.path.toLowerCase() === '/regiao/list' || to.path.toLowerCase() === '/regiao/new' || to.path.toLowerCase() === '/regiao/edit' || to.path.toLowerCase() === '/curso/list' || to.path.toLowerCase() === '/curso/new' || to.path.toLowerCase() === '/curso/edit') && token != 'admin-token'){
      next({ path: '/' })
      Message({
        message: 'Somente administradores podem acessar o caminho ' + to.path,
        type: 'error',
        duration: 4 * 1000
      })
    }else if (store.getters.roles.length === 0) {
      store.dispatch('GetInfo').then(res => { // 拉取用户信息
        next()
      }).catch((err) => {
        store.dispatch('FedLogOut').then(() => {
          Message.error(err || 'Verification failed, please login again')
          next({ path: '/' })
        })
      })
    }
    else {
      next()
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next(`/login?redirect=${to.path}`) // 否则全部重定向到登录页
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  NProgress.done() // 结束Progress
})
