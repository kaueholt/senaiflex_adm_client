import Cookies from 'js-cookie'

const TokenKey = 'vue_admin_senai_token'
const PessoaKey = 'vue_admin_senai_pessoa'
const NomeKey = 'vue_admin_senai_nome'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getId() {
  return Cookies.get(PessoaKey)
}

export function setId(idPessoa) {
  return Cookies.set(PessoaKey, idPessoa)
}

export function removeId() {
  return Cookies.remove(PessoaKey)
}

export function getName() {
  return Cookies.get(NomeKey)
}

export function setName(nome) {
  return Cookies.set(NomeKey, nome)
}

export function removeName() {
  return Cookies.remove(NomeKey)
}
