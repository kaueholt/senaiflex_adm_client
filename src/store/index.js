import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
// import exo from './modules/exo'
import pais from '@/modules/pais/store'
import uf from '@/modules/uf/store'
import cidade from '@/modules/cidade/store'
import regiao from '@/modules/regiao/store'
import microRegiao from '@/modules/microRegiao/store'
import sede from '@/modules/sede/store'
import pessoa from '@/modules/pessoa/store'
import conteudo from '@/modules/conteudo/store'
import area from '@/modules/area/store'
import categoria from '@/modules/categoria/store'
import proposta from '@/modules/proposta/store'
import industria from '@/modules/industria/store'
import curso from '@/modules/curso/store'
import videoQuantidadeMaxima from '@/modules/videoQuantidadeMaxima/store'
import getters from './getters'
// import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

// const vuexLocal = new VuexPersistence({
//   storage: window.localStorage
// })

const store = new Vuex.Store({
  modules: {
    app,
    user,
    // exo,
    pais,
    uf,
    cidade,
    regiao,
    microRegiao,
    sede,
    pessoa,
    conteudo,
    categoria,
    proposta,
    area,
    industria,
    videoQuantidadeMaxima,
    curso
  },
  getters
  // plugins: [vuexLocal.plugin]
})

export default store
